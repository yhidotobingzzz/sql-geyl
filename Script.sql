CREATE DATABASE sales_afternoon;
USE sales_afternoon;

CREATE TABLE suppliers (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL
);

CREATE TABLE goods (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL,
     price DECIMAL NOT NULL,
     id_supplier BIGINT UNSIGNED NOT NULL,
     FOREIGN KEY (id_supplier) REFERENCES suppliers(id)
);

CREATE TABLE customers (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL
);

CREATE TABLE transactions (
    id BIGINT UNSIGNED auto_increment NOT NULL PRIMARY KEY,
    id_good BIGINT UNSIGNED NOT NULL,
    id_customer BIGINT UNSIGNED NOT NULL,
    FOREIGN KEY (id_good) REFERENCES goods(id),
    FOREIGN KEY (id_customer) REFERENCES customers(id),
    time DATETIME DEFAULT now() NOT NULL,
    quantity INT NOT NULL,
    total DECIMAL NOT NULL
);

-- Insert data
INSERT INTO customers(name) VALUES ('Gilang')
INSERT INTO customers(name) VALUES ('Freddy'), ('Rangga')

INSERT INTO suppliers(name) VALUES ('Amri'), ('Yhido'), ("Adib")

INSERT INTO goods(name, price, id_supplier) VALUES
	('Pepsodent', 14500, 1),
	("Lifeboy", 24600, 2),
	("Clear", 44500, 3)
	
INSERT INTO transactions(id_good, id_customer, quantity, total) VALUES 
	(1, 1, 1, 14500),
	(2, 2, 2, 49200),
	(3, 3, 3, 133500)
